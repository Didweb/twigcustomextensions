<?php
namespace DidWeb\TwigCustomExtensions;

use DidWeb\TwigCustomExtensions\DidClasses\Slugs;
use DidWeb\TwigCustomExtensions\DidClasses\DiffDates;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class DidWebExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('slug', [$this, 'makeSlug'])
        ];
    }



    /*
    * With paramters ... {{ 'á é í ó ú'|slug({ 'delimiter': '#'}) }} --->> a#e#i#o#u
    * Parameter Options => default values:
    * 'delimiter'   => '-'
    * 'limit'       => null
    * 'lowercase'   => true
    * 'replacements'  => array()
    * 'transliterate' => true
    */
    public function makeSlug($str, $options = array())
    {
      $slugs = new Slugs();
      return $slugs->slugUrlsAction($str, $options);
    }


}
